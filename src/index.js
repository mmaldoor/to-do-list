import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { HashRouter, Route } from 'react-router-dom';

import { Alert, NavBar } from './widgets';
import { createHashHistory } from 'history';
import TaskList from './taskList';
import TaskDetails from './taskDetails';
import TaskEdit from './taskEdit';
import TaskNew from './taskNew';
import CategoryList from './categoryList';
import CategoryDetails from './categoryDetails';
import CategoryNew from './categoryNew';
import TaskCompleted from './taskCompleted';
import TaskCompletedDetails from './taskCompletedDetails';
import CategoryEdit from './categoryEdit';

const history = createHashHistory();

/**
 * Renders Menu component
 *
 * Returns NavBar, NavBar.Link to completed tasks and categorys
 */

class Menu extends Component {
  render() {
    this.state = { disabled: true };
    return (
      <NavBar brand=" Task list">
        <NavBar.Link to="/completed">Completed tasks</NavBar.Link>

        <NavBar.Link to="/categorys">Categories</NavBar.Link>
      </NavBar>
    );
  }
}

/**
 * Renders Home
 *
 * Renders TaskList
 */

class Home extends Component {
  render() {
    return (
      <div>
        <TaskList />
      </div>
    );
  }
}

/**
 * Renders Alert, Hashrouter and routes to all components in the DOM
 */

ReactDOM.render(
  <div>
    <Alert />
    <HashRouter>
      <Menu />
      <Route exact path="/" component={Home} />
      <Route exact path="/tasks" component={TaskList} />
      <Route exact path="/tasks/:id" component={TaskDetails} />
      <Route exact path="/tasks/:id/new" component={TaskNew} />
      <Route exact path="/tasks/:id/edit" component={TaskEdit} />

      <Route exact path="/completed" component={TaskCompleted} />
      <Route exact path="/completed/:id" component={TaskCompletedDetails} />

      <Route exact path="/categorys" component={CategoryList} />
      <Route exact path="/categorys/:id" component={CategoryDetails} />
      <Route exact path="/categorys/:id/new" component={CategoryNew} />
      <Route exact path="/categorys/:id/edit" component={CategoryEdit} />
    </HashRouter>
  </div>,
  document.getElementById('root')
);
