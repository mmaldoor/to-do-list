import * as React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';
import { taskService, newTaskService, categoryService } from './services';
import { Alert, Card, Row, Column, Button, Icon } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders TaskList component
 *
 * Methods:
 *
 * delete() -> Deletes task and refreshes TaskList
 *
 * complete() -> runs adddate() to add completed date to the task, generates the same task in the CompletedTasks database. Deletes the task from the Tasks database
 *
 * adddate() -> Generates date and time at the moment it is executed.
 *
 * dateSort() -> Sorts tasks by deadline
 *
 * prioSort() -> Sorts tasks by priority (first click, all tasks with priority first)
 *
 * sortAlfa() -> Sorts task names alfabetically
 *
 * prioSave() -> Saves the value of priority into the database when the priority value is changed
 *
 */

class TaskList extends Component {
  tasks = [];
  newTasks = [];
  categorys = [];
  onlyPressedOnce = false;
  onlyPressedOncePrio = false;
  onlyPressedOnceDeadline = false;
  onlyPressedOnceCategory = false;

  render() {
    return (
      <div>
        <Card>
          <Row>
            <Column>
              &nbsp; &nbsp; &nbsp; Task-name <Icon.Alfa onClick={this.sortAlfa} />
            </Column>
            <Column>
              Priority <Icon.Prio onClick={this.prioSort} />
            </Column>
            <Column>
              Category <Icon.Alfa onClick={this.sortCategory} />
            </Column>
            <Column>
              Deadline <Icon.Prio onClick={this.dateSort} />
            </Column>
            <Column></Column>
          </Row>
          <br />

          {this.tasks.map((task, i) => (
            <div className="taskdiv" key={i}>
              <Row key={task.id}>
                <Column title="Task-name" key={i}>
                  <NavLink className="myitem" to={'/tasks/' + task.id}>
                    {task.name}
                  </NavLink>
                </Column>
                <Column>
                  {task.priority ? (
                    <div title="Press to untoggle priority" alt="Task priority">
                      <Icon.Priority
                        onClick={() => {
                          // Changes the priority when the star icon is pressed
                          task.priority = 0;
                          this.prioSave(task);
                        }}
                        style={{ marginTop: '95%' }}
                      />
                    </div>
                  ) : (
                    <div title="Press to toggle priority" alt="Task priority">
                      <Icon.NoPriority
                        onClick={() => {
                          // Changes the priority when the star icon is pressed
                          task.priority = 1;
                          this.prioSave(task);
                        }}
                        style={{ marginTop: '95%' }}
                      />
                    </div>
                  )}
                </Column>
                <Column title="Category">
                  <div style={{ marginTop: '23%' }}>
                    {this.categorys.map((cate) => (task.categoryId == cate.id ? cate.name : ' '))}{' '}
                    {/*Addes which category each task belongs to */}
                  </div>
                </Column>
                <Column>
                  <div style={{ marginTop: '10%' }}>{task.endDate}</div>
                </Column>
                <Column>
                  <Button.Small
                    onClick={() => {
                      this.adddate(task);
                      this.complete(task);
                    }}
                  >
                    Complete
                  </Button.Small>
                  &nbsp; &nbsp; &nbsp;
                  <div
                    alt="Task delete button"
                    title="Press to delete"
                    style={{ display: 'inline' }}
                  >
                    {' '}
                    <Icon.Trash
                      style={{ marginTop: '7%' }}
                      onClick={() => {
                        this.delete(task);
                      }}
                    />
                  </div>
                </Column>
              </Row>
            </div>
          ))}
        </Card>

        <button
          onClick={() => {
            this.categorys.length == 0
              ? Alert.danger('You need to add atleast one category to create a new task')
              : history.push('/tasks/:id/new');
          }}
          className="new-task"
        >
          New task
        </button>
      </div>
    );
  }

  mounted() {
    taskService
      .getTasks()
      .then((tasks) => {
        this.tasks = tasks;
        categoryService.getCategorys().then((category) => {
          this.categorys = category;
        });
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  delete(task) {
    taskService
      .deleteTask(task)
      .then(TaskList.instance().mounted)
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  complete(task) {
    newTaskService
      .completeTask(task)
      .then(() => {
        taskService
          .deleteTask(task)
          .then(TaskList.instance().mounted)
          .catch((error) => {
            Alert.danger(error.message);
          });
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  prioSave(task) {
    taskService
      .updatePrio(task)
      .then()
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  dateSort() {
    if (this.onlyPressedOnceDeadline) {
      this.tasks.sort(function (a, b) {
        if (a.endDate > b.endDate) {
          return -1;
        }
        if (a.endDate < b.endDate) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceDeadline = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.endDate < b.endDate) {
          return -1;
        }
        if (a.endDate > b.endDate) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceDeadline = true;
    }
  }

  prioSort() {
    if (this.onlyPressedOncePrio) {
      this.tasks.sort(function (a, b) {
        if (a.priority < b.priority) {
          return -1;
        }
        if (a.priority > b.priority) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOncePrio = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.priority > b.priority) {
          return -1;
        }
        if (a.priority < b.priority) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOncePrio = true;
    }
  }

  sortCategory() {
    if (this.onlyPressedOnceCategory) {
      this.tasks.sort(function (a, b) {
        if (a.categoryId > b.categoryId) {
          return -1;
        }
        if (a.categoryId < b.categoryId) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceCategory = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.categoryId < b.categoryId) {
          return -1;
        }
        if (a.categoryId > b.categoryId) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceCategory = true;
    }
  }

  sortAlfa() {
    if (this.onlyPressedOnce) {
      this.tasks.sort(function (a, b) {
        if (a.name > b.name) {
          return -1;
        }
        if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnce = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnce = true;
    }
  }

  adddate(task) {
    var date = new Date();

    var months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    var Completedates = String(
      ` ${date.getFullYear()}-${months[date.getMonth()]}-${('0' + date.getDate()).slice(-2)} ${
        // Addes a 0 and showes only the first 2 digits
        ('0' + date.getHours()).slice(-2)
      }:${('0' + date.getMinutes()).slice(-2)}`
    );
    task.Completedate = Completedates;
  }
}

export default TaskList; // Exports this Componant to be used in index.js
