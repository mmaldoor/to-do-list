import * as React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';

/**
 * Renders an information card using Bootstrap classes
 *
 * Properties: title
 */
export class Card extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{this.props.title}</h5>
          <div className="card-text">{this.props.children}</div>
        </div>
      </div>
    );
  }
}

/**
 * Renders a row using Bootstrap classes.
 */
export class Row extends Component {
  render() {
    return <div className="row">{this.props.children}</div>;
  }
}

/**
 * Renders a column with specified width using Bootstrap classes.
 *
 * Properties: width, right
 */
export class Column extends Component {
  render() {
    return (
      <div
        style={this.props.style}
        className={'col' + (this.props.width ? '-' + this.props.width : '')}
      >
        <div className={'float-' + (this.props.right ? 'end' : 'start')}>{this.props.children}</div>
      </div>
    );
  }
}

/**
 * Renders a success button using Bootstrap styles.
 *
 * Properties: onClick
 */
class ButtonSuccess extends Component {
  render() {
    return (
      <button type="button" className="btn btn-outline-success" onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

/**
 * Renders a danger button using Bootstrap styles.
 *
 * Properties: onClick
 */
class ButtonDanger extends Component {
  render() {
    return (
      <button type="button" className="btn btn-outline-danger" onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

/**
 * Renders a light button using Bootstrap styles.
 *
 * Properties: onClick
 */
class ButtonLight extends Component {
  render() {
    return (
      <button type="button" className="btn btn-outline-secondary" onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

/**
 * Renders a medium light button using Bootstrap styles.
 *
 * Properties: onClick
 */

class ButtonSmall extends Component {
  render() {
    return (
      <button
        type="button"
        className="btn btn-outline-secondary btn-m"
        onClick={this.props.onClick}
      >
        {this.props.children}
      </button>
    );
  }
}

/**
 * Renders a button using Bootstrap styles.
 *
 * Properties: onClick
 */
export class Button {
  static Success = ButtonSuccess;
  static Danger = ButtonDanger;
  static Light = ButtonLight;
  static Small = ButtonSmall;
}

/**
 * Renders a NavBar link using Bootstrap styles.
 *
 * Properties: to
 */
class NavBarLink extends Component {
  render() {
    return (
      <NavLink className="nav-link" activeClassName="active" to={this.props.to}>
        {this.props.children}
      </NavLink>
    );
  }
}

/**
 * Renders a NavBar using Bootstrap classes.
 *
 * Properties: brand
 */
export class NavBar extends Component {
  static Link = NavBarLink;

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <NavLink className="navbar-brand" activeClassName="active" exact to="/">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              fill="currentColor"
              className="bi bi-card-checklist"
              viewBox="0 0 16 16"
            >
              <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z" />
              <path d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z" />{' '}
            </svg>
            {this.props.brand}
          </NavLink>
          <div className="navbar-nav">{this.props.children}</div>
        </div>
      </nav>
    );
  }
}

/**
 * Renders a bootstrap arrow a/b icon
 *
 * Properties onClick
 */
class SortAlfabetically extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-sort-alpha-down"
        viewBox="0 0 16 16"
        onClick={this.props.onClick}
      >
        <path d="M10.082 5.629 9.664 7H8.598l1.789-5.332h1.234L13.402 7h-1.12l-.419-1.371h-1.781zm1.57-.785L11 2.687h-.047l-.652 2.157h1.351z" />
        <path d="M12.96 14H9.028v-.691l2.579-3.72v-.054H9.098v-.867h3.785v.691l-2.567 3.72v.054h2.645V14zM4.5 2.5a.5.5 0 0 0-1 0v9.793l-1.146-1.147a.5.5 0 0 0-.708.708l2 1.999.007.007a.497.497 0 0 0 .7-.006l2-2a.5.5 0 0 0-.707-.708L4.5 12.293V2.5z" />
      </svg>
    );
  }
}

/**
 * Renders a bootstrap arrow icon
 *
 * Properties onClick, style
 */

class SortPrio extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-sort-down"
        viewBox="0 0 16 16"
        onClick={this.props.onClick}
        style={this.props.style}
      >
        <path d="M3.5 2.5a.5.5 0 0 0-1 0v8.793l-1.146-1.147a.5.5 0 0 0-.708.708l2 1.999.007.007a.497.497 0 0 0 .7-.006l2-2a.5.5 0 0 0-.707-.708L3.5 11.293V2.5zm3.5 1a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zM7.5 6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zm0 3a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1h-3zm0 3a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1z" />
      </svg>
    );
  }
}

/**
 * Renders a bootstrap filled star icon
 *
 * Properties onClick, style
 */

class FilledStar extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-star-fill"
        viewBox="0 0 16 16"
        onClick={this.props.onClick}
        style={this.props.style}
        tabIndex="10"
      >
        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
      </svg>
    );
  }
}

/**
 * Renders a bootstrap empty star icon
 *
 * Properties onClick, style
 */

class EmptyStar extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-star"
        viewBox="0 0 16 16"
        onClick={this.props.onClick}
        style={this.props.style}
        tabIndex="1"
      >
        <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
      </svg>
    );
  }
}

/**
 * Renders a bootstrap arrow 1/2 icon
 *
 * Properties onClick
 */

class SortNumeric extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-sort-numeric-down-alt"
        viewBox="0 0 16 16"
        onClick={this.props.onClick}
      >
        <path d="M11.36 7.098c-1.137 0-1.708-.657-1.762-1.278h1.004c.058.223.343.45.773.45.824 0 1.164-.829 1.133-1.856h-.059c-.148.39-.57.742-1.261.742-.91 0-1.72-.613-1.72-1.758 0-1.148.848-1.836 1.973-1.836 1.09 0 2.063.637 2.063 2.688 0 1.867-.723 2.848-2.145 2.848zm.062-2.735c.504 0 .933-.336.933-.972 0-.633-.398-1.008-.94-1.008-.52 0-.927.375-.927 1 0 .64.418.98.934.98z" />
        <path d="M12.438 8.668V14H11.39V9.684h-.051l-1.211.859v-.969l1.262-.906h1.046zM4.5 2.5a.5.5 0 0 0-1 0v9.793l-1.146-1.147a.5.5 0 0 0-.708.708l2 1.999.007.007a.497.497 0 0 0 .7-.006l2-2a.5.5 0 0 0-.707-.708L4.5 12.293V2.5z" />
      </svg>
    );
  }
}

/**
 * Renders a bootstrap trashcan icon
 *
 * Properties onClick, style, title
 */

class TrashSymbol extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="20"
        height="20"
        fill="currentColor"
        className="bi bi-trash"
        viewBox="0 0 16 16"
        onClick={this.props.onClick}
        style={this.props.style}
        title={this.props.title}
        tabIndex="1"
      >
        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
      </svg>
    );
  }
}

/**
 * Renders icon components using Bootstrap styles.
 */

export class Icon extends Component {
  static Alfa = SortAlfabetically;
  static Priority = FilledStar;
  static NoPriority = EmptyStar;
  static Prio = SortPrio;
  static Numeric = SortNumeric;
  static Trash = TrashSymbol;
}

/**
 * Renders a form label using Bootstrap styles.
 */
class FormLabel extends Component {
  render() {
    return <label className="col-form-label">{this.props.children}</label>;
  }
}

/**
 * Renders a form label for checkbox using Bootstrap styles.
 */

class LabelCheck extends Component {
  render() {
    return (
      <label className="form-check-label" htmlFor="flexCheckDefault">
        {this.props.children}
      </label>
    );
  }
}

/**
 * Renders a form label for textarea using Bootstrap styles.
 */
class LabelTextarea extends Component {
  render() {
    return <label htmlFor="floatingTextarea">{this.props.children}</label>;
  }
}

/**
 * Renders a form input using Bootstrap styles.
 *
 * Properties: type, value, onChange, required, pattern, id
 */
class FormInput extends Component {
  render() {
    return (
      <div>
        <input
          className="form-control"
          type={this.props.type}
          value={this.props.value}
          onChange={this.props.onChange}
          required={this.props.required}
          pattern={this.props.pattern}
          id={this.props.id}
        />
      </div>
    );
  }
}

/**
 * Renders a form textarea using Bootstrap styles.
 *
 * Properties: type, value, onChange, required, pattern, placeholder, id, className
 */

class FormTextarea extends Component {
  render() {
    return (
      <div className="form-floating">
        <textarea
          placeholder="Write task description here"
          // id="floatingTextarea"
          id={this.props.id}
          value={this.props.value}
          defaultValue={this.props.defaultValue}
          onChange={this.props.onChange}
          className="form-control"
          style={this.props.style}
          required={this.props.required}
        ></textarea>
        <label htmlFor="floatingTextarea">{this.props.children}</label>
      </div>
    );
  }
}

/**
 * Renders a form checkbox using Bootstrap styles.
 *
 * Properties: onChange, checked
 */

class FormCheckbox extends Component {
  render() {
    return (
      <div className="form-check">
        <input
          onChange={this.props.onChange}
          type="checkbox"
          className="form-check-input"
          id="flexCheckDefault"
          checked={this.props.checked}
          value=""
        />
      </div>
    );
  }
}
class FormSwitch extends Component {
  render() {
    return (
      <div className="form-check form-switch">
        <input
          onChange={this.props.onChange}
          className="form-check-input"
          type="checkbox"
          id="flexSwitchCheckDefault"
          checked={this.props.checked}
        />
      </div>
    );
  }
}

/**
 * Renders a form selectusing Bootstrap styles.
 *
 * Properties: value, onChange
 */

class FormSelect extends Component {
  render() {
    return (
      <div>
        <select
          className="form-select"
          aria-label="Default select example"
          onChange={this.props.onChange}
          value={this.props.value}
        ></select>
      </div>
    );
  }
}

/**
 * Renders form components using Bootstrap styles.
 */
export class Form {
  static Label = FormLabel;
  static Input = FormInput;
  static Checkbox = FormCheckbox;
  static CheckLabel = LabelCheck;
  static Textarea = FormTextarea;
  static LabelArea = LabelTextarea;
  static Select = FormSelect;
  static Switch = FormSwitch;
}

/**
 * Renders alert messages using Bootstrap classes.
 *
 */
export class Alert extends Component {
  alerts = [];
  nextId = 0;

  render() {
    return (
      <div>
        {this.alerts.map((alert, i) => (
          <div
            key={alert.id}
            className={'alert alert-dismissible alert-' + alert.type}
            role="alert"
          >
            {alert.text}
            <button
              type="button"
              className="btn-close btn-sm"
              onClick={() => this.alerts.splice(i, 1)}
            />
          </div>
        ))}
      </div>
    );
  }

  /**
   * Show success alert.
   */
  static success(text) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      let instance = Alert.instance(); // Get rendered Alert component instance
      if (instance) instance.alerts.push({ id: instance.nextId++, text: text, type: 'success' });
    });
  }

  /**
   * Show info alert.
   */
  static info(text) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      let instance = Alert.instance(); // Get rendered Alert component instance
      if (instance) instance.alerts.push({ id: instance.nextId++, text: text, type: 'info' });
    });
  }

  /**
   * Show warning alert.
   */
  static warning(text) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      let instance = Alert.instance(); // Get rendered Alert component instance
      if (instance) instance.alerts.push({ id: instance.nextId++, text: text, type: 'warning' });
    });
  }

  /**
   * Show danger alert.
   */
  static danger(text) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      let instance = Alert.instance(); // Get rendered Alert component instance
      if (instance) instance.alerts.push({ id: instance.nextId++, text: text, type: 'danger' });
    });
  }
}
