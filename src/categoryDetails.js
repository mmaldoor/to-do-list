import * as React from 'react';
import { Component } from 'react-simplified';
import { categoryService, completedTaskService } from './services';
import { Alert, Card, Row, Column, Button } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders CategoryDetails Component
 *
 * Methods:
 * delete() -> Deletes Category and all tasks in that category, if there are no tasks a catch deletes only the category.
 *
 * edit() -> Sends you to the CategoryEdit Component
 *
 * warning() -> Sends an Alert warning the first time it is called, calls delete() the second time
 */

class CategoryDetails extends Component {
  category = null;
  tasks = [];
  categorys = [];
  IsCalledOnce = true;

  render() {
    if (!this.category) return null;

    return (
      <div>
        <Card title="Category Details">
          <Row>
            <Column width={2}>Name:</Column>
            <Column>{this.category.name}</Column>
          </Row>
          <Row>
            <Column width={2}>Tasks:</Column>
            <Column>{this.tasks.length}</Column>
          </Row>
          <Row>
            <Column width={2}>Description:</Column>
            <Column> {this.category.description}</Column>
          </Row>
        </Card>
        <Card title="Tasks in this category">
          <ul>
            {this.tasks.map((
              task // Map function that lists all the tasks for a specifc category, inside that category.
            ) => (
              <li key={task.id}>{task.name}</li>
            ))}
          </ul>
        </Card>
        <Card>
          <Row>
            <Column left>
              <Button.Light onClick={this.edit}>Edit</Button.Light>
            </Column>
            <Column right>
              <Button.Danger onClick={this.warning}>Delete category</Button.Danger>
            </Column>
          </Row>
        </Card>
      </div>
    );
  }

  mounted() {
    categoryService
      .getCategory(this.props.match.params.id)
      .then((category) => {
        this.category = category;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });

    categoryService
      .getTasks(this.props.match.params.id)
      .then((tasks) => {
        this.tasks = tasks;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });

    categoryService
      .getCategorys()
      .then((categorys) => {
        this.categorys = categorys;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  delete() {
    completedTaskService
      .deleteTasks(this.category)
      .then(() => {
        completedTaskService.deleteCompletedTasks(this.category).then(() => {
          categoryService
            .deleteCategory(this.category)
            .then(() => {
              history.push('/categorys/');
            })
            .catch((error) => {
              Alert.danger(error.message);
            });
        });
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  warning() {
    if (this.IsCalledOnce) {
      Alert.danger(
        'Deleting this category will delete all subsequent tasks! If you are sure, press delete again.'
      );
      this.IsCalledOnce = false;
    } else {
      this.delete();
      this.IsCalledOnce = true;
    }
  }

  edit() {
    history.push('/categorys/ ' + this.category.id + ' /edit'); // Opens the edit page for that specific task
  }
}

export default CategoryDetails; // Exports this Componant to be used in index.js
